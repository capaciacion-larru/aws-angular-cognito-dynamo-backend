import AWS from 'aws-sdk';
import {global as env} from '../appconfig'
AWS.config.update({
    region: env.awsConfig.region,
    accessKeyId: env.awsConfig.accessKeyId,
    secretAccessKey: env.awsConfig.secretAccessKey
  });
export default AWS