import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { Persona } from 'src/models';
import { PersonaService } from './persona.service';

@Controller("persona")
export class PersonaController {
  constructor(private readonly service: PersonaService) {}

  @Get("/:dni")
  async getPersona(@Param('dni') dni:string) {
    return this.service.getPersona(dni);
  }

  @Get()
  async getPersonas() {
    return this.service.getPersonas();
  }
  @Post()
  async create(@Body() persona:Persona) {
    console.log("controller")
    console.log(persona)
    return this.service.create(persona);
  }
  @Put()
  async update(@Body() persona:Persona) {
    return this.service.update(persona);
  }
}
