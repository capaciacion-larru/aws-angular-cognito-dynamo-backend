import { Module } from '@nestjs/common';
import { PersonaController } from './persona.controller';
import { PersonaRepository } from './persona.repository';
import { PersonaService } from './persona.service';

@Module({
  controllers: [PersonaController],
  providers: [
    PersonaService,
    PersonaRepository
  ]
})
export class PersonaModule {}
