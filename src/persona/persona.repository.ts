import { Injectable } from '@nestjs/common';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import {conn} from '../config/dynamoConnect'
import {Persona} from '../models'
@Injectable()
export class PersonaRepository {
    dynamo:DocumentClient
    constructor(){
        this.dynamo=conn()
    }
  async getPersona(dni:string) {
    const params = {
        TableName : "Persona",
        Key: {
            id: dni
          }
      }
      console.log(params)
      const data = await this.dynamo.get(params).promise();
    return data;
  }
  async getPersonas() {
    const params = {
        TableName : "Persona"
      }
      const data = await (await this.dynamo.scan(params).promise()).Items;
    return data;
  }
  
  async create(persona:Persona) {
    return (await this.dynamo.put({
        TableName: "Persona",
        Item: {
            id:persona.dni,
            nombre:persona.nombre,
            foto:persona.foto
        } 
    })).promise()
  }
  
  async update(persona:Persona) {
    return (await this.dynamo.put({
        TableName: "Persona",
        Item: {
            id:persona.dni,
            nombre:persona.nombre,
            foto:persona.foto
        } 
    })).promise()
  }
}
