import { Injectable } from '@nestjs/common';
import { Persona } from 'src/models';
import { PersonaRepository} from './persona.repository'
@Injectable()
export class PersonaService {
  constructor(private repository:PersonaRepository){}
  async getPersona(dni:string) {
    return await this.repository.getPersona(dni);
  }

  async getPersonas() {
    return await this.repository.getPersonas();
  }
  async create(persona:Persona) {
    return await this.repository.create(persona);
  }
  async update(persona:Persona) {
    return await this.repository.update(persona);
  }
}
