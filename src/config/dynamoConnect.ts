import AWS from './awsConfig'
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
export function conn():DocumentClient{
    return new AWS.DynamoDB.DocumentClient();
}